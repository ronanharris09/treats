import { backtrack, split } from "../../../../src"

describe("backtrack processing test", () => {
    test("backtrack test one", () => {
        expect(
            backtrack(split([
                {
                    current: 'A',
                    next: 'B',
                    distance: 3,
                    index: {
                        x: 0,
                        y: 1,
                    }
                },
                {
                    current: 'A',
                    next: 'C',
                    distance: 4,
                    index: {
                        x: 0,
                        y: 2,
                    }
                },
                {
                    current: 'B',
                    next: 'C',
                    distance: 3,
                    index: {
                        x: 1,
                        y: 2,
                    }
                },
                {
                    current: 'B',
                    next: 'D',
                    distance: 5,
                    index: {
                        x: 1,
                        y: 3,
                    }
                },
                {
                    current: 'B',
                    next: 'E',
                    distance: 2,
                    index: {
                        x: 1,
                        y: 4,
                    }
                },
                {
                    current: 'C',
                    next: 'E',
                    distance: 6,
                    index: {
                        x: 2,
                        y: 4,
                    }
                },
                {
                    current: 'C',
                    next: 'E',
                    distance: 6,
                    index: {
                        x: 2,
                        y: 4,
                    }
                },
                {
                    current: 'D',
                    next: 'E',
                    distance: 1,
                    index: {
                        x: 3,
                        y: 4,
                    }
                },
                {
                    current: 'D',
                    next: 'F',
                    distance: 3,
                    index: {
                        x: 3,
                        y: 5,
                    }
                },
                {
                    current: 'E',
                    next: 'F',
                    distance: 1,
                    index: {
                        x: 4,
                        y: 5,
                    }
                },
                {
                    current: 'E',
                    next: 'F',
                    distance: 1,
                    index: {
                        x: 4,
                        y: 5,
                    }
                },
                {
                    current: 'E',
                    next: 'F',
                    distance: 1,
                    index: {
                        x: 4,
                        y: 5,
                    }
                },
                {
                    current: 'E',
                    next: 'F',
                    distance: 1,
                    index: {
                        x: 4,
                        y: 5,
                    }
                },
            ], "A", "F"))
        ).toStrictEqual(
            [
                [
                    { current: 'A', next: 'B', distance: 3, index: { x: 0, y: 1 } },
                    { current: 'B', next: 'D', distance: 5, index: { x: 1, y: 3 } },
                    { current: 'D', next: 'F', distance: 3, index: { x: 3, y: 5 } }
                ],
                [
                    { current: 'A', next: 'B', distance: 3, index: { x: 0, y: 1 } },
                    { current: 'B', next: 'E', distance: 2, index: { x: 1, y: 4 } },
                    { current: 'E', next: 'F', distance: 1, index: { x: 4, y: 5 } }
                ],
                [
                    { current: 'A', next: 'B', distance: 3, index: { x: 0, y: 1 } },
                    { current: 'B', next: 'C', distance: 3, index: { x: 1, y: 2 } },
                    { current: 'C', next: 'E', distance: 6, index: { x: 2, y: 4 } },
                    { current: 'E', next: 'F', distance: 1, index: { x: 4, y: 5 } }
                ],
                [
                    { current: 'A', next: 'C', distance: 4, index: { x: 0, y: 2 } },
                    { current: 'C', next: 'E', distance: 6, index: { x: 2, y: 4 } },
                    { current: 'E', next: 'F', distance: 1, index: { x: 4, y: 5 } }
                ],
                [
                    { current: 'A', next: 'B', distance: 3, index: { x: 0, y: 1 } },
                    { current: 'B', next: 'D', distance: 5, index: { x: 1, y: 3 } },
                    { current: 'D', next: 'E', distance: 1, index: { x: 3, y: 4 } },
                    { current: 'E', next: 'F', distance: 1, index: { x: 4, y: 5 } }
                ]
            ]
        )
    })
})

describe("split processing test", () => {
    test("split test one", () => {
        expect(
            split(
                [
                    {
                        current: 'A',
                        next: 'B',
                        distance: 3,
                        index: {
                            x: 0,
                            y: 1,
                        }
                    },
                    {
                        current: 'A',
                        next: 'C',
                        distance: 4,
                        index: {
                            x: 0,
                            y: 2,
                        }
                    },
                    {
                        current: 'B',
                        next: 'C',
                        distance: 3,
                        index: {
                            x: 1,
                            y: 2,
                        }
                    },
                    {
                        current: 'B',
                        next: 'D',
                        distance: 5,
                        index: {
                            x: 1,
                            y: 3,
                        }
                    },
                    {
                        current: 'B',
                        next: 'E',
                        distance: 2,
                        index: {
                            x: 1,
                            y: 4,
                        }
                    },
                    {
                        current: 'C',
                        next: 'E',
                        distance: 6,
                        index: {
                            x: 2,
                            y: 4,
                        }
                    },
                    {
                        current: 'C',
                        next: 'E',
                        distance: 6,
                        index: {
                            x: 2,
                            y: 4,
                        }
                    },
                    {
                        current: 'D',
                        next: 'E',
                        distance: 1,
                        index: {
                            x: 3,
                            y: 4,
                        }
                    },
                    {
                        current: 'D',
                        next: 'F',
                        distance: 3,
                        index: {
                            x: 3,
                            y: 5,
                        }
                    },
                    {
                        current: 'E',
                        next: 'F',
                        distance: 1,
                        index: {
                            x: 4,
                            y: 5,
                        }
                    },
                    {
                        current: 'E',
                        next: 'F',
                        distance: 1,
                        index: {
                            x: 4,
                            y: 5,
                        }
                    },
                    {
                        current: 'E',
                        next: 'F',
                        distance: 1,
                        index: {
                            x: 4,
                            y: 5,
                        }
                    },
                    {
                        current: 'E',
                        next: 'F',
                        distance: 1,
                        index: {
                            x: 4,
                            y: 5,
                        }
                    },
                ],
                "A", "F"
            )
        ).toStrictEqual(
            {
                initial: [
                    {
                        current: 'A',
                        next: 'B',
                        distance: 3,
                        index: {
                            x: 0,
                            y: 1,
                        }
                    },
                    {
                        current: 'A',
                        next: 'C',
                        distance: 4,
                        index: {
                            x: 0,
                            y: 2,
                        }
                    }
                ],
                between: [
                    {
                        current: 'B',
                        next: 'C',
                        distance: 3,
                        index: {
                            x: 1,
                            y: 2,
                        }
                    },
                    {
                        current: 'B',
                        next: 'D',
                        distance: 5,
                        index: {
                            x: 1,
                            y: 3,
                        }
                    },
                    {
                        current: 'B',
                        next: 'E',
                        distance: 2,
                        index: {
                            x: 1,
                            y: 4,
                        }
                    },
                    {
                        current: 'C',
                        next: 'E',
                        distance: 6,
                        index: {
                            x: 2,
                            y: 4,
                        }
                    },
                    {
                        current: 'C',
                        next: 'E',
                        distance: 6,
                        index: {
                            x: 2,
                            y: 4,
                        }
                    },
                    {
                        current: 'D',
                        next: 'E',
                        distance: 1,
                        index: {
                            x: 3,
                            y: 4,
                        }
                    }
                ],
                final: [
                    {
                        current: 'D',
                        next: 'F',
                        distance: 3,
                        index: {
                            x: 3,
                            y: 5,
                        }
                    },
                    {
                        current: 'E',
                        next: 'F',
                        distance: 1,
                        index: {
                            x: 4,
                            y: 5,
                        }
                    },
                    {
                        current: 'E',
                        next: 'F',
                        distance: 1,
                        index: {
                            x: 4,
                            y: 5,
                        }
                    },
                    {
                        current: 'E',
                        next: 'F',
                        distance: 1,
                        index: {
                            x: 4,
                            y: 5,
                        }
                    },
                    {
                        current: 'E',
                        next: 'F',
                        distance: 1,
                        index: {
                            x: 4,
                            y: 5,
                        }
                    }
                ]
            }
        )
    })
})