import { explore } from "../../../../src"

const graph_one = [
    {
        current: 'A',
        next: 'B',
        distance: 3,
        index: {
            x: 0,
            y: 1,
        }
    },
    {
        current: 'A',
        next: 'C',
        distance: 4,
        index: {
            x: 0,
            y: 2,
        }
    },
    {
        current: 'B',
        next: 'A',
        distance: 3,
        index: {
            x: 1,
            y: 0,
        }
    },
    {
        current: 'B',
        next: 'C',
        distance: 3,
        index: {
            x: 1,
            y: 2,
        }
    },
    {
        current: 'B',
        next: 'D',
        distance: 5,
        index: {
            x: 1,
            y: 3,
        }
    },
    {
        current: 'B',
        next: 'E',
        distance: 2,
        index: {
            x: 1,
            y: 4,
        }
    },
    {
        current: 'C',
        next: 'A',
        distance: 4,
        index: {
            x: 2,
            y: 0,
        }
    },
    {
        current: 'C',
        next: 'B',
        distance: 3,
        index: {
            x: 2,
            y: 1,
        }
    },
    {
        current: 'C',
        next: 'E',
        distance: 6,
        index: {
            x: 2,
            y: 4,
        }
    },
    {
        current: 'D',
        next: 'B',
        distance: 5,
        index: {
            x: 3,
            y: 1,
        }
    },
    {
        current: 'D',
        next: 'E',
        distance: 1,
        index: {
            x: 3,
            y: 4,
        }
    },
    {
        current: 'D',
        next: 'F',
        distance: 3,
        index: {
            x: 3,
            y: 5,
        }
    },
    {
        current: 'E',
        next: 'B',
        distance: 2,
        index: {
            x: 4,
            y: 1,
        }
    },
    {
        current: 'E',
        next: 'C',
        distance: 6,
        index: {
            x: 4,
            y: 2,
        }
    },
    {
        current: 'E',
        next: 'D',
        distance: 1,
        index: {
            x: 4,
            y: 3,
        }
    },
    {
        current: 'E',
        next: 'F',
        distance: 1,
        index: {
            x: 4,
            y: 5,
        }
    },
    {
        current: 'F',
        next: 'D',
        distance: 3,
        index: {
            x: 5,
            y: 3,
        }
    },
    {
        current: 'F',
        next: 'E',
        distance: 1,
        index: {
            x: 5,
            y: 4,
        }
    },
]

describe("graph exploration tests", () => {
    test("dummy graph one test A-F", () => {
        expect(
            explore(
                graph_one, 'A', 'F'
            ))
            .toStrictEqual(
                [
                    {
                        current: 'A',
                        next: 'B',
                        distance: 3,
                        index: {
                            x: 0,
                            y: 1,
                        }
                    },
                    {
                        current: 'A',
                        next: 'C',
                        distance: 4,
                        index: {
                            x: 0,
                            y: 2,
                        }
                    },
                    {
                        current: 'B',
                        next: 'C',
                        distance: 3,
                        index: {
                            x: 1,
                            y: 2,
                        }
                    },
                    {
                        current: 'B',
                        next: 'D',
                        distance: 5,
                        index: {
                            x: 1,
                            y: 3,
                        }
                    },
                    {
                        current: 'B',
                        next: 'E',
                        distance: 2,
                        index: {
                            x: 1,
                            y: 4,
                        }
                    },
                    {
                        current: 'C',
                        next: 'E',
                        distance: 6,
                        index: {
                            x: 2,
                            y: 4,
                        }
                    },
                    {
                        current: 'C',
                        next: 'E',
                        distance: 6,
                        index: {
                            x: 2,
                            y: 4,
                        }
                    },
                    {
                        current: 'D',
                        next: 'E',
                        distance: 1,
                        index: {
                            x: 3,
                            y: 4,
                        }
                    },
                    {
                        current: 'D',
                        next: 'F',
                        distance: 3,
                        index: {
                            x: 3,
                            y: 5,
                        }
                    },
                    {
                        current: 'E',
                        next: 'F',
                        distance: 1,
                        index: {
                            x: 4,
                            y: 5,
                        }
                    },
                    {
                        current: 'E',
                        next: 'F',
                        distance: 1,
                        index: {
                            x: 4,
                            y: 5,
                        }
                    },
                    {
                        current: 'E',
                        next: 'F',
                        distance: 1,
                        index: {
                            x: 4,
                            y: 5,
                        }
                    },
                    {
                        current: 'E',
                        next: 'F',
                        distance: 1,
                        index: {
                            x: 4,
                            y: 5,
                        }
                    },
                ]
            )
    })

    test("dummy graph two test A-E", () => {
        expect(
            explore(
                graph_one, 'A', 'E'
            ))
            .toStrictEqual(
                [
                    {
                        current: 'A',
                        next: 'B',
                        distance: 3,
                        index: {
                            x: 0,
                            y: 1,
                        }
                    },
                    {
                        current: 'A',
                        next: 'C',
                        distance: 4,
                        index: {
                            x: 0,
                            y: 2,
                        }
                    },
                    {
                        current: 'B',
                        next: 'C',
                        distance: 3,
                        index: {
                            x: 1,
                            y: 2,
                        }
                    },
                    {
                        current: 'B',
                        next: 'D',
                        distance: 5,
                        index: {
                            x: 1,
                            y: 3,
                        }
                    },
                    {
                        current: 'B',
                        next: 'E',
                        distance: 2,
                        index: {
                            x: 1,
                            y: 4,
                        }
                    },
                    {
                        current: 'C',
                        next: 'E',
                        distance: 6,
                        index: {
                            x: 2,
                            y: 4,
                        }
                    },
                    {
                        current: 'C',
                        next: 'E',
                        distance: 6,
                        index: {
                            x: 2,
                            y: 4,
                        }
                    },
                    {
                        current: 'D',
                        next: 'E',
                        distance: 1,
                        index: {
                            x: 3,
                            y: 4,
                        }
                    },
                    {
                        current: 'D',
                        next: 'F',
                        distance: 3,
                        index: {
                            x: 3,
                            y: 5,
                        }
                    },
                    {
                        current: 'F',
                        next: 'E',
                        distance: 1,
                        index: {
                            x: 5,
                            y: 4,
                        }
                    },
                ]
            )
    })
})