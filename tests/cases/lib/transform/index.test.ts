import { matrix } from "../../../../src"

describe("matrix processing library tests", () => {
    test("matrix input with node names", () => {
        expect(matrix([[Infinity, 4, 5], [4, Infinity, Infinity], [5, Infinity, Infinity]], ['A', 'B', 'C'])).toStrictEqual([{current: 'A', next: 'B', distance: 4, index: {x: 0, y: 1}}, {current: 'A', next: 'C', distance: 5, index: {x: 0, y: 2}}, {current: 'B', next: 'A', distance: 4, index: {x: 1, y: 0}}, {current: 'C', next: 'A', distance: 5, index: {x: 2, y: 0}}])
    })

    test("matrix input without node names", () => {
        expect(matrix([[Infinity, 4, 5], [4, Infinity, Infinity], [5, Infinity, Infinity]])).toStrictEqual([{current: '1', next: '2', distance: 4, index: {x: 0, y: 1}}, {current: '1', next: '3', distance: 5, index: {x: 0, y: 2}}, {current: '2', next: '1', distance: 4, index: {x: 1, y: 0}}, {current: '3', next: '1', distance: 5, index: {x: 2, y: 0}}])
    })

    test("matrix input without node names and negative numbers", () => {
        expect(matrix([[-1, 4, 5], [4, Infinity, -3], [5, -7, Infinity]])).toStrictEqual([{current: '1', next: '2', distance: 4, index: {x: 0, y: 1}}, {current: '1', next: '3', distance: 5, index: {x: 0, y: 2}}, {current: '2', next: '1', distance: 4, index: {x: 1, y: 0}}, {current: '3', next: '1', distance: 5, index: {x: 2, y: 0}}])
    })

    test("matrix input without node names, zeroes, and negative numbers", () => {
        expect(matrix([[-1, 4, 5], [4, 0, -3], [5, -7, 0]])).toStrictEqual([{current: '1', next: '2', distance: 4, index: {x: 0, y: 1}}, {current: '1', next: '3', distance: 5, index: {x: 0, y: 2}}, {current: '2', next: '1', distance: 4, index: {x: 1, y: 0}}, {current: '3', next: '1', distance: 5, index: {x: 2, y: 0}}])
    })
})