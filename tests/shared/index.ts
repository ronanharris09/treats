/**
 *  @license GPL-2.0-only
 *  Copyright (C) 2021 Ronan Harris
 *  SPDX-License-Identifier: GPL-2.0-only
 *  Source: git+https://gitlab.com/ronanharris09/treats.git
 */

import { Pair, PairGroup } from "../../src"


// Dummy Matrix Data

export const matrix_a_data: Array<Array<number>> = [
    [  0,  3,  4, -1, -1, -1 ],
    [  3,  0,  3,  5,  2, -1 ],
    [  4,  3,  0, -1,  6, -1 ],
    [ -1,  5, -1,  0,  1,  3 ],
    [ -1,  2,  6,  1,  0,  1 ],
    [ -1, -1, -1,  3,  1,  0 ],
]

export const matrix_b_data: Array<Array<number>> = [
    [    0,  450,  950,  800,   -1,   -1,   -1,   -1 ],
    [  450,    0,   -1,   -1, 1800,  900,   -1,   -1 ],
    [  950,   -1,    0,   -1, 1100,  600,   -1,   -1 ],
    [  800,   -1,   -1,    0,   -1,  600, 1200,   -1 ],
    [   -1, 1800, 1100,   -1,    0,  900,   -1,  400 ],
    [   -1,  900,  600,  600,  900,    0, 1000, 1300 ],
    [   -1,   -1,   -1, 1200,   -1, 1000,    0,  600 ],
    [   -1,   -1,   -1,   -1,  400, 1300,  600,    0 ],
]


// Dummy Matrix Name

export const matrix_a_name: Array<string> = [ "A", "B", "C", "D", "E", "F" ]
export const matrix_b_name: Array<string> = [ "A", "B", "C", "D", "E", "F", "G", "H" ]


// Dummy Pair Data

export const pair_a_data: Array<Pair> = [
    {
        current: 'A',
        next: 'B',
        distance: 3,
        index: {
            x: 0,
            y: 1,
        }
    },
    {
        current: 'A',
        next: 'C',
        distance: 4,
        index: {
            x: 0,
            y: 2,
        }
    },
    {
        current: 'B',
        next: 'A',
        distance: 3,
        index: {
            x: 1,
            y: 0,
        }
    },
    {
        current: 'B',
        next: 'C',
        distance: 3,
        index: {
            x: 1,
            y: 2,
        }
    },
    {
        current: 'B',
        next: 'D',
        distance: 5,
        index: {
            x: 1,
            y: 3,
        }
    },
    {
        current: 'B',
        next: 'E',
        distance: 2,
        index: {
            x: 1,
            y: 4,
        }
    },
    {
        current: 'C',
        next: 'A',
        distance: 4,
        index: {
            x: 2,
            y: 0,
        }
    },
    {
        current: 'C',
        next: 'B',
        distance: 3,
        index: {
            x: 2,
            y: 1,
        }
    },
    {
        current: 'C',
        next: 'E',
        distance: 6,
        index: {
            x: 2,
            y: 4,
        }
    },
    {
        current: 'D',
        next: 'B',
        distance: 5,
        index: {
            x: 3,
            y: 1,
        }
    },
    {
        current: 'D',
        next: 'E',
        distance: 1,
        index: {
            x: 3,
            y: 4,
        }
    },
    {
        current: 'D',
        next: 'F',
        distance: 3,
        index: {
            x: 3,
            y: 5,
        }
    },
    {
        current: 'E',
        next: 'B',
        distance: 2,
        index: {
            x: 4,
            y: 1,
        }
    },
    {
        current: 'E',
        next: 'C',
        distance: 6,
        index: {
            x: 4,
            y: 2,
        }
    },
    {
        current: 'E',
        next: 'D',
        distance: 1,
        index: {
            x: 4,
            y: 3,
        }
    },
    {
        current: 'E',
        next: 'F',
        distance: 1,
        index: {
            x: 4,
            y: 5,
        }
    },
    {
        current: 'F',
        next: 'D',
        distance: 3,
        index: {
            x: 5,
            y: 3,
        }
    },
    {
        current: 'F',
        next: 'E',
        distance: 1,
        index: {
            x: 5,
            y: 4,
        }
    },
]


// Dummy Pair Exploration Data

export const exploration_pair_data_af: Array<Pair> = [
    {
        current: 'A',
        next: 'B',
        distance: 3,
        index: {
            x: 0,
            y: 1,
        }
    },
    {
        current: 'A',
        next: 'C',
        distance: 4,
        index: {
            x: 0,
            y: 2,
        }
    },
    {
        current: 'B',
        next: 'C',
        distance: 3,
        index: {
            x: 1,
            y: 2,
        }
    },
    {
        current: 'B',
        next: 'D',
        distance: 5,
        index: {
            x: 1,
            y: 3,
        }
    },
    {
        current: 'B',
        next: 'E',
        distance: 2,
        index: {
            x: 1,
            y: 4,
        }
    },
    {
        current: 'C',
        next: 'E',
        distance: 6,
        index: {
            x: 2,
            y: 4,
        }
    },
    {
        current: 'C',
        next: 'E',
        distance: 6,
        index: {
            x: 2,
            y: 4,
        }
    },
    {
        current: 'D',
        next: 'E',
        distance: 1,
        index: {
            x: 3,
            y: 4,
        }
    },
    {
        current: 'D',
        next: 'F',
        distance: 3,
        index: {
            x: 3,
            y: 5,
        }
    },
    {
        current: 'E',
        next: 'F',
        distance: 1,
        index: {
            x: 4,
            y: 5,
        }
    },
    {
        current: 'E',
        next: 'F',
        distance: 1,
        index: {
            x: 4,
            y: 5,
        }
    },
    {
        current: 'E',
        next: 'F',
        distance: 1,
        index: {
            x: 4,
            y: 5,
        }
    },
    {
        current: 'E',
        next: 'F',
        distance: 1,
        index: {
            x: 4,
            y: 5,
        }
    },
]

export const exploration_pair_data_ae: Array<Pair> = [
    {
        current: 'A',
        next: 'B',
        distance: 3,
        index: {
            x: 0,
            y: 1,
        }
    },
    {
        current: 'A',
        next: 'C',
        distance: 4,
        index: {
            x: 0,
            y: 2,
        }
    },
    {
        current: 'B',
        next: 'C',
        distance: 3,
        index: {
            x: 1,
            y: 2,
        }
    },
    {
        current: 'B',
        next: 'D',
        distance: 5,
        index: {
            x: 1,
            y: 3,
        }
    },
    {
        current: 'B',
        next: 'E',
        distance: 2,
        index: {
            x: 1,
            y: 4,
        }
    },
    {
        current: 'C',
        next: 'E',
        distance: 6,
        index: {
            x: 2,
            y: 4,
        }
    },
    {
        current: 'C',
        next: 'E',
        distance: 6,
        index: {
            x: 2,
            y: 4,
        }
    },
    {
        current: 'D',
        next: 'E',
        distance: 1,
        index: {
            x: 3,
            y: 4,
        }
    },
    {
        current: 'D',
        next: 'F',
        distance: 3,
        index: {
            x: 3,
            y: 5,
        }
    },
    {
        current: 'F',
        next: 'E',
        distance: 1,
        index: {
            x: 5,
            y: 4,
        }
    },
]


// Dummy Pair Split Data

export const split_pair_data_af: PairGroup = {
    initial: [
        {
            current: 'A',
            next: 'B',
            distance: 3,
            index: {
                x: 0,
                y: 1,
            }
        },
        {
            current: 'A',
            next: 'C',
            distance: 4,
            index: {
                x: 0,
                y: 2,
            }
        }
    ],
    between: [
        {
            current: 'B',
            next: 'C',
            distance: 3,
            index: {
                x: 1,
                y: 2,
            }
        },
        {
            current: 'B',
            next: 'D',
            distance: 5,
            index: {
                x: 1,
                y: 3,
            }
        },
        {
            current: 'B',
            next: 'E',
            distance: 2,
            index: {
                x: 1,
                y: 4,
            }
        },
        {
            current: 'C',
            next: 'E',
            distance: 6,
            index: {
                x: 2,
                y: 4,
            }
        },
        {
            current: 'C',
            next: 'E',
            distance: 6,
            index: {
                x: 2,
                y: 4,
            }
        },
        {
            current: 'D',
            next: 'E',
            distance: 1,
            index: {
                x: 3,
                y: 4,
            }
        }
    ],
    final: [
        {
            current: 'D',
            next: 'F',
            distance: 3,
            index: {
                x: 3,
                y: 5,
            }
        },
        {
            current: 'E',
            next: 'F',
            distance: 1,
            index: {
                x: 4,
                y: 5,
            }
        },
        {
            current: 'E',
            next: 'F',
            distance: 1,
            index: {
                x: 4,
                y: 5,
            }
        },
        {
            current: 'E',
            next: 'F',
            distance: 1,
            index: {
                x: 4,
                y: 5,
            }
        },
        {
            current: 'E',
            next: 'F',
            distance: 1,
            index: {
                x: 4,
                y: 5,
            }
        }
    ]
}


// Dummy Pair Backtrack Data

export const backtrack_pair_data_af = [
    [
        {
            current: 'A',
            next: 'B',
            distance: 3,
            index: {
                x: 0,
                y: 1
            }
        },
        {
            current: 'B',
            next: 'D',
            distance: 5,
            index: {
                x: 1,
                y: 3
            }
        },
        {
            current: 'D',
            next: 'F',
            distance: 3,
            index: {
                x: 3,
                y: 5
            }
        }
    ],
    [
        {
            current: 'A',
            next: 'B',
            distance: 3,
            index: {
                x: 0,
                y: 1
            }
        },
        {
            current: 'B',
            next: 'E',
            distance: 2,
            index: {
                x: 1,
                y: 4
            }
        },
        {
            current: 'E',
            next: 'F',
            distance: 1,
            index: {
                x: 4,
                y: 5
            }
        }
    ],
    [
        {
            current: 'A',
            next: 'B',
            distance: 3,
            index: {
                x: 0,
                y: 1
            }
        },
        {
            current: 'B',
            next: 'C',
            distance: 3,
            index: {
                x: 1,
                y: 2
            }
        },
        {
            current: 'C',
            next: 'E',
            distance: 6,
            index: {
                x: 2,
                y: 4
            }
        },
        {
            current: 'E',
            next: 'F',
            distance: 1,
            index: {
                x: 4,
                y: 5
            }
        }
    ],
    [
        {
            current: 'A',
            next: 'C',
            distance: 4,
            index: {
                x: 0,
                y: 2
            }
        },
        {
            current: 'C',
            next: 'E',
            distance: 6,
            index: {
                x: 2,
                y: 4
            }
        },
        {
            current: 'E',
            next: 'F',
            distance: 1,
            index: {
                x: 4,
                y: 5
            }
        }
    ],
    [
        {
            current: 'A',
            next: 'B',
            distance: 3,
            index: {
                x: 0,
                y: 1
            }
        },
        {
            current: 'B',
            next: 'D',
            distance: 5,
            index: {
                x: 1,
                y: 3
            }
        },
        {
            current: 'D',
            next: 'E',
            distance: 1,
            index: {
                x: 3,
                y: 4
            }
        },
        {
            current: 'E',
            next: 'F',
            distance: 1,
            index: {
                x: 4,
                y: 5
            }
        }
    ]
]