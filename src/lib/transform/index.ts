/**
 *  @license GPL-2.0-only
 *  Copyright (C) 2021 Ronan Harris
 *  SPDX-License-Identifier: GPL-2.0-only
 *  Source: git+https://gitlab.com/ronanharris09/treats.git
 */

import { Pair } from "../../types";

/**
 * @description transforms a two dimensional array into a structured array of `Pair`s.
 * @param {Array<Array<Pair>>} matrix a required property that accepts a 2 dimensional array of positive numbers.
 * @param {Array<string>} nodes an optional propery that accepts an array of string of every node names known, otherwise, it will use the index + 1 of the row and collumn from matrix.
 * @returns {Array<Pair>} returns an array of structured Pair objects
 */
export const matrix = (matrix: Array<Array<number>>, names: Array<string> = ['']) : Array<Pair> => {

    let pairs: Array<Pair> = Array()

    matrix.map((row, row_index) => {
        row.map((collumn, collumn_index) => {
            if (collumn > 0) {
                if (collumn < Infinity) {
                    pairs.push(
                        {
                            current: names[row_index] || `${row_index + 1}`, 
                            next: names[collumn_index] || `${collumn_index + 1}`, 
                            distance: collumn,
                            index: {
                                x: row_index,
                                y: collumn_index
                            }
                        }
                    )
                }
            }
        })
    })

    return pairs
}