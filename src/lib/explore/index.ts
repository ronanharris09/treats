/**
 *  @license GPL-2.0-only
 *  Copyright (C) 2021 Ronan Harris
 *  SPDX-License-Identifier: GPL-2.0-only
 *  Source: git+https://gitlab.com/ronanharris09/treats.git
 */

import { Pair } from "../../types";

/**
 * @description process an array of `Pair`s to build another structure that can be futher processed by backtrack.
 * @param {Array<Pair>} graph a required property that accepts an array of Pair objects that represents a graph ready to explore
 * @param {string} starting_node a required property that accepts a string representing a node name marked as the initial point.
 * @param {string} destination_node a required property that accepts a string representing a node name marked as the final point.
 * @returns {Array<Pair>} returns an array of Pair objects.
 */
export const explore = (graph: Array<Pair>,  initial_name: string, final_name: string) : Array<Pair> => {

    let queue: Array<Pair> = graph.filter(item => item.current === initial_name)
    let visited: Array<Pair> = Array()

    while (queue.length > 0) {
        let pair = queue.shift()
        
        if (pair) {
            queue.push(...graph.filter(item => item.current === pair!.next && item.next !== pair!.current && item.current !== final_name))
            visited.map(pair => {
                queue = queue.filter(item => item.next !== pair.current)
            })
            visited.push(pair)
        }
    }
    
    return visited
}