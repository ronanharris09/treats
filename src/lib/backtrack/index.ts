/**
 *  @license GPL-2.0-only
 *  Copyright (C) 2021 Ronan Harris
 *  SPDX-License-Identifier: GPL-2.0-only
 *  Source: git+https://gitlab.com/ronanharris09/treats.git
 */

import { Pair, PairGroup } from "../../types";

/**
 * @description process an array of `Pair`s to build another structure that can be futher processed by backtrack.
 * @param {PairGroup} graph a required property that accepts an array of Pair objects that represents a graph ready to explore
 * @returns {Array<Array<Pair>} returns a two dimensional array of Pair objects where each array within the array contain `Pair`s that can be treated as a single route.
 */
export const backtrack = (graph: PairGroup): Array<Array<Pair>> => {
    let known_routes = Array<Array<Pair>>()
    
    while (graph.final.length > 0) {
        let new_route = Array<Pair>()
        const current_node = graph.final.shift()
        
        if (current_node) {
            new_route.push(current_node)
            let next_node = graph.between.find(node => node.next === current_node.current)
            
            while (graph.initial.filter(node => node.next === new_route[new_route.length - 1].current).length === 0 || graph.between.filter(node => node.next === new_route[new_route.length - 1].current).length > 0) {
                if (next_node) {
                    new_route.push(next_node)

                    if (graph.between.filter(node => node.current === next_node?.current).length >= graph.final.length) {
                        graph.between.splice(graph.between.indexOf(next_node), 1)
                    }
                }

                next_node = graph.between.find(node => node.next === next_node?.current)
            }

            new_route.push(graph.initial.find(node => node.next === new_route[new_route.length - 1].current) as Pair)
        }

        known_routes.push(new_route.reverse())
    }

    return known_routes
}

/**
 * @description splitting an Array of `Pair`s into three different groups depending given a Pair object `current` and `next` value against `initial_name` and `destination_name`.
 * @param {Array<Pair>} graph A required Array of `Pair`s to process.
 * @param {string} initial_name A required string that represents the initial node to begin.
 * @param {string} destination_name A required string that represents the destination node to reach.
 * @returns {PairGroup} An object that consists of `initial`, `between`, and `final` prop where each props are an Array of `Pair`s.
 */
export const split = (graph: Array<Pair>, initial_name: string, destination_name: string): PairGroup => {
    let result: PairGroup = {
        initial: Array<Pair>(),
        between: Array<Pair>(),
        final: Array<Pair>(),
    }

    graph.map(item => {
        if (item.current === initial_name) {
            result.initial.push(item)
            return
        }

        if (item.next === destination_name) {
            result.final.push(item)
            return
        }

        result.between.push(item)
    })

    return result
}