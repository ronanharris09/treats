export interface Indexes {
    x: number,
    y: number
}

export interface Pair {
    current: string,
    next: string,
    distance: number,
    index: Indexes,
}

export interface PairGroup {
    initial: Array<Pair>,
    between: Array<Pair>,
    final: Array<Pair>,
}